﻿using System.Text;
using IdentityModel.OidcClient;
using LoginMaui.Auth0;

namespace LoginMaui;

public partial class MainPage : ContentPage
{
    private readonly Auth0Client auth0Client;
    private bool LoggedIn;

    public MainPage(Auth0Client client)
    {
        InitializeComponent();
        auth0Client = client;
        LoggedIn = false;
    }

    private async void OnLoginClicked(object sender, EventArgs e)
    {
        var loginResult = await auth0Client.LoginAsync();

        if (!loginResult.IsError)
        {
            LoggedIn = true;

            LoginView.IsVisible = false;
            HomeView.IsVisible = true;

            UsernameLbl.Text = loginResult.User.Identity.Name;

            var imageSource = loginResult.User
              .Claims.FirstOrDefault(c => c.Type == "picture")?.Value;

            if (!string.IsNullOrEmpty(imageSource))
            {
                UserPictureImg.Source = imageSource;
                UserPictureImg.IsVisible = false;
            }
            else
            {
                UserPictureImg.IsVisible = false;
            }


        }
        else
        {
            //await DisplayAlert("Error", loginResult.ErrorDescription, "OK");
            LoggedIn = false;
            LoginView.IsVisible = true;
            HomeView.IsVisible = false;
        }
    }

    private async void OnLogoutClicked(object sender, EventArgs e)
    {
        var logoutResult = await auth0Client.LogoutAsync();

        if (!logoutResult.IsError)
        {
            LoginView.IsVisible = true;
            HomeView.IsVisible = false;
        }
        else
        {
            //await DisplayAlert("Error", loginResult.ErrorDescription, "OK");
            LoginView.IsVisible = false;
            HomeView.IsVisible = true;
        }
    }



}


