﻿using System.Reflection;
using LoginMaui.Auth0;
using Microsoft.Extensions.Configuration;

namespace LoginMaui;

public static class MauiProgram
{
    public static MauiApp CreateMauiApp()
    {
        var builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
            });

        builder.Services.AddSingleton<MainPage>();
        builder.Configuration.AddUserSecrets(Assembly.GetExecutingAssembly(), true);

        var domain = builder.Configuration["Auth0:Domain"];
        var clientId = builder.Configuration["Auth0:ClientId"];
        var scope = builder.Configuration["Auth0:Scope"];
        var redirectUri = builder.Configuration["Auth0:RedirectUri"];

        builder.Services.AddSingleton(new Auth0Client(new()
        {
            Domain = domain,
            ClientId = clientId,
            Scope = scope,
            RedirectUri = redirectUri
        }));
        return builder.Build();
    }
}
